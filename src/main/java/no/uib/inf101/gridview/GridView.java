package no.uib.inf101.gridview;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;
public class GridView extends JPanel{
    private static final double OUTERMARGIN = 30;
    private static final Color MARGINCOLOR = Color.LIGHT_GRAY;
    private static final int XMAX = 400;
    private static final int YMAX = 300;

    int margin = 30;
    int rader = 3;
    int kolonner = 4;
    IColorGrid ruter ;
    Rectangle2D  box ;


    public GridView(IColorGrid ruter) {
        this.setPreferredSize(new Dimension(XMAX, YMAX));
        this.ruter = ruter;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        drawGrid(g2);

    }
    private void drawGrid(Graphics2D g2) {
        double margin = 30;
        double x = OUTERMARGIN;
        double y = OUTERMARGIN;
        double width = XMAX - 2*OUTERMARGIN;
        double height = YMAX - 2*OUTERMARGIN;
        g2.setColor(MARGINCOLOR);
        g2.fill(new Rectangle2D.Double(x, y, width, height));
        List<Color> actualList = new ArrayList<>();
        List<CellColor> actList = new ArrayList<>();

        Color addColor ;
        for ( int i=0 ; i < rader*kolonner ; i++) {
            int row = i / kolonner;
            int col = i % kolonner;
            CellPosition cp = new CellPosition( row, col);
            addColor = ruter.get(cp);
            //    System.out.println(addColor);
            actualList.add(i, addColor) ;  // actualList er et CellColorCollection object
            //    System.out.println(actualList.get(i));
            //      System.out.println( " lengde " + actualList.size());
        }
        actList = ruter.getCells();

        // System.out.println(actList.size());
        CellPositionToPixelConverter conver = new  CellPositionToPixelConverter( box, ruter,  margin);
        CellColorCollection collection = new CellColorCollection(actualList);

        drawCells(g2, collection, conver);
    }


    private  void drawCells(Graphics2D g2, CellColorCollection collection, CellPositionToPixelConverter conver) {  // CellColorCollection coll ?
        Rectangle2D rect = new Rectangle2D.Double(0,0,300,400);
        List<Color> samling = new ArrayList<Color>();
        samling = collection.getActualList();
        int kolonner = 4;
        int row = 0;
        int col = 0;
        //  System.out.println( " lengde " + actualList.size());
        for (  int i= 0; i<samling.size(); i++){
            row = i / kolonner;
            col = i % kolonner;
            CellPosition cp = new CellPosition(row, col);
            Rectangle2D rectt = getBoundForCell(cp);
            Color actualColor = samling.get(i);
            //    System.out.println(actualColor);
            g2.setColor(actualColor);
            g2.fill(rectt);



        }


    }

    public  Rectangle2D getBoundForCell(CellPosition cp) {

        CellPositionToPixelConverter convt = new  CellPositionToPixelConverter( box, ruter,  margin);
        Rectangle2D result = convt.getBoundsForCell(cp);

        return result;
    }
}

