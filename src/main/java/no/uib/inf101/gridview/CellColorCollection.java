package no.uib.inf101.gridview;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class CellColorCollection {
    List<Color> actualList = new ArrayList<>();

    public CellColorCollection(List<Color> actualList) {
        this.actualList = actualList;
    }
    public List<Color> getActualList() {
        return this.actualList;
    }
    public void setActualList(List<Color> collection) {
        this.actualList = collection;
    }
}
