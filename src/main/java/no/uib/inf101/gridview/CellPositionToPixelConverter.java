package no.uib.inf101.gridview;

import javax.swing.Box;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;
import java.awt.geom.Rectangle2D;

public class CellPositionToPixelConverter {

    double margin;
    Rectangle2D  box ;
    GridDimension gd ;
    double width = 400.0;
    int kolonner = 4;
    int rader = 3;
    double height = 300.0;

    public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin){
        this.margin = margin;
        this.box = box;
        this.gd = gd;
    }

    public Rectangle2D getBoundsForCell(CellPosition cp) {
        double[] adresseX = new double[rader*kolonner];
        double[] adresseY = new double[rader*kolonner];
        double cellHeight = (height -6*margin ) / rader;
        double cellWidth = (width -7*margin)/ kolonner;

        for ( int i = 0; i < kolonner; i++ ){
            double ii = (int) i % kolonner;
            adresseX[i] = (2+ii)*margin + ii*cellWidth;
            adresseX[4+i] = (2+ii)*margin + ii*cellWidth;
            adresseX[8+i] = (2+ii)*margin + ii*cellWidth;

        }
        for ( int j= 0; j < kolonner; j++ ){
            double jj = (int) j % rader;
            adresseY[j] = 2*margin ;
            adresseY[j+4] = 3*margin + 1*cellHeight;
            adresseY[j+8] = 4*margin + 2*cellHeight;
        }


        int row = cp.row();

        int col = cp.col();
        int ind = row*kolonner +  col ;
        // System.out.println("ind " + ind + "  "  +adresseX[ind] + "  " + adresseY[ind]);
        return new Rectangle2D.Double(adresseX[ind], adresseY[ind], cellWidth, cellHeight);
    }


}

