package no.uib.inf101.colorgrid;


import java.util.ArrayList;
import java.util.List;
import java.awt.Color;

public class ColorGrid implements IColorGrid, GridDimension, CellColorCollection {

    private  int rader;
    private  int kolonner;
    CellPosition pos ;
    List<Color> gridList = new ArrayList<>();
    private Color none;
    Color initColor ; // Color.DARK_GRAY;
    String initValue = null;

    List<CellColor> cellList = new ArrayList<>();
    CellColor celle  ;
    Color color =  Color.RED;





    public ColorGrid( int rader, int kolonner) {
        this.rader = rader;
        this.kolonner = kolonner;
        initier();
    }

    private void initier() {
        for ( int i= 0; i < rader*kolonner; i++) {
            //  int indeks = i / kolonner + rader ;
            gridList.add(i,initColor);
        }

    }

    /**
     * Get the color of the cell at the given position.
     *
     * @param pos the position
     * @return the color of the cell
     * @throws IndexOutOfBoundsException if the position is out of bounds
     */
    @Override
    public Color get(CellPosition pos){
        int row = pos.row();
        int col = pos.col();
        int indeks = row*kolonner +  col ;
     //   if (indeks >= 0 && indeks < rader*kolonner && row  < rows() && col < cols()) {
        if ( !(row >= 0 && row < rows() & col >= 0 && col < cols())){
            throw new IndexOutOfBoundsException("Indeks out of bounds");
        }

        Color resultat;
        resultat = gridList.get(indeks);
   //     System.out.println(resultat);
        return gridList.get(indeks);



    }

    /**
     * Set the color of the cell at the given position.
     *
     * @param pos the position
     * @param color the new color
     * @throws IndexOutOfBoundsException if the position is out of bounds
     */
    @Override
    public void set(CellPosition pos, Color color){

        int row = pos.row();
        int col = pos.col();
        int indeks = row*kolonner +  col ;
        if ( !(row >= 0 && row < rows() & col >= 0 && col < cols())){
            throw new IndexOutOfBoundsException("Indeks out of bounds");
        }
        gridList.set(indeks, color);

    }
    /** Number of rows in the grid.. */
    @Override
    public int rows(){
        return rader;
    }

    /** Number of columns in the grid. */
    @Override
    public int cols(){
        return kolonner;
    }



    /**
     * Get a list containing the GridCell objects in this collection
     *
     * @return a list of all GridCell objects in this collection
     */
    @Override
    public List<CellColor> getCells() {
        for ( int i = 0 ; i < kolonner*rader ; i++) {
                int row = i / kolonner;
                int col = i % kolonner;
                celle = new CellColor(new CellPosition(row, col) , gridList.get(i));
                cellList.add(0, celle);
        }
        return cellList;
    }

}

